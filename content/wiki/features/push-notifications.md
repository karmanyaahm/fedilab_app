*[Home](../../home) > [Features](../../features) &gt;* Push notifications

<div align="center">
<img src="../../res/fedilab_icon.png" width="70px">
<b>Fedilab</b>
</div>

## Push notifications

Since version 2.38.0, Fedilab now works with [push notifications](https://en.wikipedia.org/wiki/Push_technology#Webpush). Live and delayed notifications have been removed.
For working, it needs a provider that will deliver notifications to the app. You are up to install your own provider, but we will explain how to make it work.

PS: This step by step is more dedicated to F-Droid users. If you use the Google Play version, this is not needed. But you can follow this guide to change your provider (by default it is Firebase).

This work has been done thanks to the help of [UnfiedPush](https://unifiedpush.org/). 


### Install the provider

First you need to install the provider. We will show how to use a version of `Gotify` that has been reworked for working with UnifiedPush.

On [F-Droid](https://www.f-droid.org/), search for `Gotify-UP`.

You now need an account on Gotify. You can use your own instance, but we also provide one where you can create your account:

[https://gotify.fedilab.app](https://gotify.fedilab.app)

Visit the previous website with your favourite browser and click on "Register". Define a username and a password.

Then on `Gotify-UP` app, put the url "https://gotify.fedilab.app" and click on "Check URL". You will have to enter your previous account credential, and then click on "CREATE".

That's done!

Now Fedilab will automatically uses that account for pushing notifications (PS, think to restart Fedilab).


