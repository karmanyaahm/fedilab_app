## Application privacy-policy

### This page is dedicated to the Fedilab privacy-policy


#### Stored data
Only basic information from connected accounts are stored locally on the device. These data remain strictly confidential and are only usable by the application. Removing the application leads to the deletion of these data.

No logins and passwords are stored locally. They are only used to get the access token. This token can be revoked at any time with Mastodon.

All connections are proceed through SSL encryptions to an instance. The application will not connect to an instance that presents a non valid certificate.

#### Required Android Permission

- `ACCESS_NETWORK_STATE` :<br>
&nbsp;&nbsp;&nbsp; *Used to know if the device is connected to the WIFI.*

- `INTERNET` :<br>
&nbsp;&nbsp;&nbsp; *Used to communicate with the API.*

- `WRITE_EXTERNAL_STORAGE` :<br>
&nbsp;&nbsp;&nbsp; *Used to store media and to move the application on the SD card*

- `READ_EXTERNAL_STORAGE` :<br>
&nbsp;&nbsp;&nbsp; *Used to pick up media for uploading them.*

- `BOOT_COMPLETED` :<br>
&nbsp;&nbsp;&nbsp; *Used to start the notification service when the device starts*

- `WAKE_LOCK` :<br>
&nbsp;&nbsp;&nbsp; *Used by the notification service*

&nbsp;
#### Authorization with the API
  - `Read` : Read data from the account
  - `Write` : Write messages
  - `Follow` : Follow, unfollow, block mute accounts

These actions are performed only by the user.

#### Tracking and Advertisements
The application does not use any tracking tools and does not run advertising.

#### Crash reports
The application uses <a href="https://github.com/ACRA/acra" target="\_blank" rel="noopener noreferrer">Acra</a> for crash reporting. This is disabled by default. When enabled from settings, after a crash, user will be asked to send an email with crash logs to the developer

#### Toot translations
The application allows to use third party translation services. <a href="https://yandex.ru" target="\_blank" rel="noopener noreferrer">Yandex</a> is used by default. The application also supports <a href="https://www.deepl.com" target="\_blank" rel="noopener noreferrer">DeepL</a> and <a href="https://translate.systran.net" target="\_blank" rel="noopener noreferrer">Systran</a>.

Their privacy policies:
- Yandex: [`yandex.ru/legal/confidential/?lang=en`](https://yandex.ru/legal/confidential/?lang=en)
- DeepL: [`www.deepl.com/privacy.html`](https://www.deepl.com/privacy.html)
- Systran: [`www.systransoft.com/systran/policies/privacy-policy`](https://www.systransoft.com/systran/policies/privacy-policy/)
